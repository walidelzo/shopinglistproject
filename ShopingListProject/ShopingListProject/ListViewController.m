//
//  ViewController.m
//  ShopingListProject
//
//  Created by Admin on 2/14/19.
//  Copyright © 2019 NanoSoft. All rights reserved.
//

#import "ListViewController.h"
#import "Classes/PrepareFirstLists.h"
@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _sharedInstance=[AppData SharedManager];
    [PrepareFirstLists prepare];
    ///make a temporary user
    UserClass *tempUser=[[UserClass alloc]
                         initWithnName:@"walid"
                         AndEmail:@"walidelzo@gmail.com"
                         AndUid:@"1985"];
    _sharedInstance.curUser=tempUser;

}

#pragma - :mark  button actions

- (IBAction)newListBtnAction:(UIButton *)sender
{

    UIAlertController *alert=[UIAlertController
                              alertControllerWithTitle:@"Add new List "
                              message:@"please enter list name"
                              preferredStyle:UIAlertControllerStyleAlert];
    
   [ alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
       textField.placeholder=@"List name" ;
       textField.font=[UIFont systemFontOfSize:20];
       textField.textAlignment=NSTextAlignmentCenter;
        
   }];
    
    UIAlertAction *action=[UIAlertAction
                           actionWithTitle:@"OK"
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action)
                           {
                               [self addNewList: alert.textFields[0].text];
                           }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:action];
    [alert addAction:actionCancel];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)offlineBtnAction:(UIButton *)sender
{

}

#pragma mark helper Method
-(void)addNewList:(NSString*)listName{
    ShopingListsClass *newList=[[ShopingListsClass alloc]initWithName:listName AndlistOwner:_sharedInstance.curUser AndSavedItems:[NSMutableArray new ]];
    [_sharedInstance.curLST addObject:newList];
    [_listTableView reloadData];
}


#pragma  - :mark tableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   // NSLog(@"%lu",_sharedInstance.curLST.count);
    return _sharedInstance.curLST.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"listCell"
                                                          forIndexPath:indexPath];
    cell.textLabel.text=_sharedInstance.curLST[indexPath.row].listName;
    NSLog(@"%@",_sharedInstance.curLST[indexPath.row].listName);
        cell.detailTextLabel.text=[NSString stringWithFormat: @"%lu items for %@",
                               _sharedInstance.curLST[indexPath.row].listItems.count,
                               _sharedInstance.curUser.name
                               
                               ];
    return cell;
}


@end
