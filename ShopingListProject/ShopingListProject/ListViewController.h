#import <UIKit/UIKit.h>
#import "Classes/AppData.h"
@interface ListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

//our outlets
@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;

//our Actions
- (IBAction)newListBtnAction:(UIButton *)sender;
- (IBAction)offlineBtnAction:(UIButton *)sender;

@property(nonatomic,retain)AppData *sharedInstance;

@end

